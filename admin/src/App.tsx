import React, { useEffect, useState } from "react";
import { invoke, view } from "@forge/bridge";
import {
  ApolloClient,
  ApolloProvider,
  NormalizedCacheObject,
} from "@apollo/client";
import Connections from "./pages/Connections";
import { createClient, Error, Loading } from "@app-reviews/shared";
import { Router, Route, Switch, __RouterContext } from "react-router";
import New from "./pages/New";

function App() {
  const [client, setClient] =
    useState<ApolloClient<NormalizedCacheObject> | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [history, setHistory] = useState<any>(null);

  useEffect(() => {
    view.createHistory().then((newHistory) => setHistory(newHistory));

    invoke("generateGraphQLToken")
      .then((token) =>
        setClient(
          createClient(token, {
            "X-Hasura-Role": "jira-admin",
          })
        )
      )
      .catch(() => setError(true))
      .then(() => setLoading(false));
  }, []);

  return (
    <>
      {error && <Error />}
      {!error && (loading || !history) && <Loading />}
      {client && (
        <ApolloProvider client={client}>
          {history && (
            <Router history={history}>
              <Switch>
                <Route path="/new">
                  <New />
                </Route>
                <Route path="/">
                  <Connections />
                </Route>
              </Switch>
            </Router>
          )}
        </ApolloProvider>
      )}
    </>
  );
}

export default App;
