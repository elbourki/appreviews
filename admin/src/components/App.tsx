import { gql, useMutation } from "@apollo/client";
import Select from "@atlaskit/select";
import React, { useRef } from "react";
import { useEffect } from "react";
import { useState } from "react";
import styles from "./App.module.scss";
import { Stores } from "@app-reviews/shared";

function useDidUpdateEffect(fn, inputs) {
  const didMountRef = useRef(false);

  useEffect(() => {
    if (didMountRef.current) return fn();
    else didMountRef.current = true;
  }, inputs);
}

function App({ app, projects }) {
  const [linked, setLinked] = useState(app.projectIds);
  const [update] = useMutation(
    gql`
      mutation UpdateApps($id: uuid!, $projectIds: jsonb) {
        update_apps_by_pk(
          pk_columns: { id: $id }
          _set: { projectIds: $projectIds }
        ) {
          projectIds
        }
      }
    `
  );

  useDidUpdateEffect(() => {
    update({
      variables: {
        id: app.id,
        projectIds: linked,
      },
    });
  }, [linked]);

  return (
    <div className={styles.app}>
      <div className={styles.app__details}>
        <div className={styles.icon__container}>
          <img className={styles.icon} src={app.icon} />
        </div>
        <div>
          <h5>{app.name}</h5>
          <small>
            {[Stores[app.store].name, app.developer]
              .filter(Boolean)
              .join(" • ")}
          </small>
        </div>
      </div>
      <div className={styles.app__projects}>
        <Select
          id={app.id}
          options={projects}
          isMulti
          placeholder="Link with a project"
          onChange={(values) => setLinked(values.map((value) => value.id))}
          value={linked.map((id) =>
            projects.find((project) => project.id === id)
          )}
        />
      </div>
    </div>
  );
}

export default App;
