import React, { useState } from "react";
import { EmptyState, Platforms } from "@app-reviews/shared";
import RefreshIcon from "@atlaskit/icon/glyph/refresh";
import UnlinkIcon from "@atlaskit/icon/glyph/unlink";
import AddIcon from "@atlaskit/icon/glyph/add";
import { LoadingButton } from "@atlaskit/button";
import styles from "./Connection.module.scss";
import { gql, useMutation } from "@apollo/client";
import { invoke } from "@forge/bridge";
import App from "./App";
import Textfield from "@atlaskit/textfield";
import Form, { Field } from "@atlaskit/form";

const accountProps = {
  name: "Account name",
  email: "Email",
  clientId: "Client ID",
};

function Connection({ connection, projects }) {
  const [syncing, setSyncing] = useState(false);
  const [adding, setAdding] = useState(false);
  const [disconnect, { loading: disconnecting }] = useMutation(
    gql`
      mutation Disconnect($id: uuid!) {
        delete_connections_by_pk(id: $id) {
          id
        }
      }
    `,
    {
      variables: {
        id: connection.id,
      },
    }
  );

  const sync = () => {
    setSyncing(true);
    invoke(`${connection.platform}Sync`, {
      id: connection.id,
    })
      .catch(() => alert("Something went wrong."))
      .then(() => setSyncing(false));
  };

  const add = (formState) => {
    setAdding(true);
    invoke(`${connection.platform}Add`, {
      id: connection.id,
      ...formState,
    })
      .then((r: any) => {
        if (r.error) throw new Error(r.error);
      })
      .catch((e) => alert(e))
      .then(() => setAdding(false));
  };

  return (
    <div key={connection.id} className={styles.connection}>
      <div className={styles.connection__header}>
        <div className={styles.platform}>
          <img
            className={styles.platform__logo}
            src={Platforms[connection.platform].icon}
          />
          <div className={styles.platform__name}>
            {Platforms[connection.platform].name}
          </div>
        </div>
        <div>
          <LoadingButton
            appearance="danger"
            onClick={() => disconnect()}
            isLoading={disconnecting}
            iconBefore={<UnlinkIcon label="Disconnect icon" size="small" />}
          >
            Disconnect
          </LoadingButton>
        </div>
      </div>
      <div className={styles.connection__details}>
        {Object.entries(connection.account)
          .map(([k, v]) => `${accountProps[k]}: ${v}`)
          .join(" • ")}{" "}
        • Linked on{" "}
        {new Date(connection.createdAt).toLocaleDateString("en-US", {
          year: "numeric",
          month: "long",
          day: "numeric",
        })}
      </div>
      <div className={styles.connection__apps}>
        <div className={styles.apps__header}>
          <h5>Apps list</h5>
          {Platforms[connection.platform].syncable ? (
            <LoadingButton
              iconBefore={<RefreshIcon label="Refresh icon" size="small" />}
              onClick={sync}
              isLoading={syncing}
            >
              Sync apps
            </LoadingButton>
          ) : (
            <Form onSubmit={add}>
              {({ formProps }: any) => (
                <form className={styles.app__form} {...formProps}>
                  <Field name="packageId" isRequired>
                    {({ fieldProps }: any) => (
                      <Textfield
                        placeholder="Package ID"
                        isCompact
                        {...fieldProps}
                      />
                    )}
                  </Field>
                  <LoadingButton
                    type="submit"
                    iconBefore={<AddIcon label="Add icon" size="small" />}
                    isLoading={adding}
                  >
                    Add app
                  </LoadingButton>
                </form>
              )}
            </Form>
          )}
        </div>
        {connection.apps.length ? (
          connection.apps.map((app) => (
            <App key={app.id} app={app} projects={projects} />
          ))
        ) : (
          <EmptyState>
            <h3>No apps have been added yet</h3>
            <p>
              {Platforms[connection.platform].syncable
                ? `After adding your apps on ${
                    Platforms[connection.platform].name
                  }, click on the sync button to import them into App Reviews.`
                : "We can't import your Google Play Console apps automatically so you'll need to add them manually."}
            </p>
          </EmptyState>
        )}
      </div>
    </div>
  );
}

export default Connection;
