import { gql, useSubscription } from "@apollo/client";
import React, { Fragment, useEffect } from "react";
import { Loading, Error, EmptyState, Link } from "@app-reviews/shared";
import PageHeader from "@atlaskit/page-header";
import Button from "@atlaskit/button";
import styles from "./Connections.module.scss";
import LinkIcon from "@atlaskit/icon/glyph/link";
import Connection from "../components/Connection";
import { useState } from "react";
import { requestJira } from "@forge/bridge";

function Connections() {
  const [projects, setProjects] = useState([]);
  const { loading, error, data } = useSubscription(
    gql`
      subscription getConnections {
        connections(order_by: { createdAt: desc }) {
          id
          platform
          account
          createdAt
          createdBy
          updatedAt
          apps(order_by: { id: desc }) {
            id
            store
            projectIds
            name
            icon
            extId
            developer
          }
        }
      }
    `
  );

  const fetchProjects = (startAt = 0) => {
    return requestJira(
      `/rest/api/3/project/search?maxResults=50&startAt=${startAt}&orderBy=lastIssueUpdatedTime&typeKey=software`
    )
      .then((response) => response.json())
      .then((json) => [
        ...json.values.map((value) => ({
          id: value.id,
          label: value.name,
          value: value.key,
        })),
        ...(json.isLast ? [] : fetchProjects(json.startAt + 50)),
      ]);
  };

  useEffect(() => {
    fetchProjects().then((results) => setProjects(results));
  }, []);

  return (
    <Fragment>
      <PageHeader
        actions={
          <Link to="/new" unstyled>
            <Button
              iconBefore={<LinkIcon label="Link icon" size="small" />}
              appearance="primary"
            >
              New connection
            </Button>
          </Link>
        }
      >
        Connected platforms
      </PageHeader>
      {error && <Error />}
      {loading && <Loading />}
      {!loading &&
        !error &&
        (data.connections.length ? (
          <div>
            {data.connections.map((connection) => (
              <Connection
                key={connection.id}
                connection={connection}
                projects={projects}
              />
            ))}
          </div>
        ) : (
          <EmptyState>
            <h3>No connected platforms</h3>
            <p>
              You haven't connected your accounts yet. Would you like to connect
              your first platform now?
            </p>
            <Link to="/new" unstyled>
              <Button
                appearance="primary"
                iconBefore={<LinkIcon label="Link icon" size="small" />}
                className={styles.emptystate__button}
              >
                Connect a platform
              </Button>
            </Link>
          </EmptyState>
        ))}
    </Fragment>
  );
}

export default Connections;
