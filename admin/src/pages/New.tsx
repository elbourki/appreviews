import React, { Fragment, useRef } from "react";
import Textfield from "@atlaskit/textfield";
import Form, { Field, HelperMessage } from "@atlaskit/form";
import Button, { LoadingButton } from "@atlaskit/button";
import ArrowLeftIcon from "@atlaskit/icon/glyph/arrow-left";
import ArrowRightIcon from "@atlaskit/icon/glyph/arrow-right";
import OpenIcon from "@atlaskit/icon/glyph/open";
import { DoneEmoji, Link, Platforms } from "@app-reviews/shared";
import { ProgressTracker } from "@atlaskit/progress-tracker";
import styles from "./New.module.scss";
import { AnimatePresence, motion } from "framer-motion";
import { useState } from "react";
import { invoke, router } from "@forge/bridge";
import SectionMessage from "@atlaskit/section-message";
import authorizeGuide from "../../assets/guides/appfigures/authorize.png";
import codeGuide from "../../assets/guides/appfigures/code.png";
import projectGuide from "../../assets/guides/play/project.png";
import oauthGuide from "../../assets/guides/play/oauth.png";
import credentialsGuide from "../../assets/guides/play/credentials.png";
import playCodeGuide from "../../assets/guides/play/code.png";

const animations = {
  animate: { opacity: 1, x: 0, y: 0 },
  exit: (custom) => ({
    opacity: 0,
    x: custom(20),
    y: 0,
  }),
  initial: (custom) => ({
    opacity: 0,
    x: custom(20),
    y: 0,
  }),
};

function Footer({ children, back }) {
  return (
    <div className={styles.form__buttons}>
      <Button
        iconBefore={<ArrowLeftIcon label="Back icon" size="small" />}
        onClick={back}
      >
        Back
      </Button>
      {children}
    </div>
  );
}

function Appfollow({ finish, back }) {
  const [submitting, setSubmitting] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const connect = (formState) => {
    setSubmitting(true);
    setError(null);
    invoke("appfollowConnect", {
      ...formState,
    })
      .then((r: any) => {
        if (r.error) throw new Error(r.error);
        finish();
      })
      .catch((e) => setError(e.message))
      .then(() => setSubmitting(false));
  };

  return (
    <Form onSubmit={connect}>
      {({ formProps }: any) => (
        <form {...formProps}>
          {error && (
            <SectionMessage title="Something went wrong" appearance="error">
              <p>{error}</p>
            </SectionMessage>
          )}
          <Field label="API Secret" name="apiSecret" isRequired>
            {({ fieldProps }: any) => (
              <Fragment>
                <Textfield
                  placeholder="Enter your API secret here"
                  {...fieldProps}
                />
                <HelperMessage>
                  This secret can be found in your&nbsp;
                  <a
                    href="https://watch.appfollow.io/settings/general"
                    onClick={(event) => {
                      event.preventDefault();
                      router.open((event.target as HTMLAnchorElement).href);
                    }}
                  >
                    AppFollow account settings.
                  </a>
                </HelperMessage>
              </Fragment>
            )}
          </Field>
          <Footer back={back}>
            <LoadingButton
              type="submit"
              iconAfter={<ArrowRightIcon label="Continue icon" size="small" />}
              appearance="primary"
              isLoading={submitting}
            >
              Connect
            </LoadingButton>
          </Footer>
        </form>
      )}
    </Form>
  );
}

function Appfigures({ finish, back }) {
  const [loading, setLoading] = useState(false);
  const [token, setToken] = useState(null);
  const [submitting, setSubmitting] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const authorize = () => {
    setLoading(true);
    invoke("appfiguresAuthorize")
      .then((data: any) =>
        router.open(data.url).then(() => setToken(data.token))
      )
      .catch(() => {})
      .then(() => setLoading(false));
  };

  const connect = (formState) => {
    setSubmitting(true);
    setError(null);
    invoke("appfiguresConnect", {
      ...formState,
      token,
    })
      .then((r: any) => {
        if (r.error) throw new Error(r.error);
        finish();
      })
      .catch((e) => setError(e.message))
      .then(() => setSubmitting(false));
  };

  return (
    <Form onSubmit={connect}>
      {({ formProps }: any) => (
        <form {...formProps}>
          {error && (
            <div className={styles.error}>
              <SectionMessage title="Something went wrong" appearance="error">
                <p>{error}</p>
              </SectionMessage>
            </div>
          )}
          {token ? (
            <>
              <img className={styles.guide__img} src={codeGuide} />
              <Field label="Your authorization code" name="code" isRequired>
                {({ fieldProps }: any) => (
                  <Fragment>
                    <Textfield
                      placeholder="Enter the code provided to you by Appfigures"
                      {...fieldProps}
                    />
                  </Fragment>
                )}
              </Field>
            </>
          ) : (
            <>
              <img className={styles.guide__img} src={authorizeGuide} />
              <p className={styles.guide__content}>
                You'll need to authorize App Reviews access to your Appfigures
                account. Once that's done you'll be presented with a code that
                you can then enter in the next step to finish connecting
                Appfigures!
              </p>
            </>
          )}
          <Footer back={back}>
            {token ? (
              <LoadingButton
                type="submit"
                iconAfter={
                  <ArrowRightIcon label="Continue icon" size="small" />
                }
                appearance="primary"
                isLoading={submitting}
              >
                Connect
              </LoadingButton>
            ) : (
              <LoadingButton
                onClick={authorize}
                iconAfter={<OpenIcon label="Open icon" size="small" />}
                appearance="primary"
                isLoading={loading}
              >
                Authorize
              </LoadingButton>
            )}
          </Footer>
        </form>
      )}
    </Form>
  );
}

function Play({ finish, back }) {
  const [credentials, setCredentials] = useState<any>(null);
  const [submitting, setSubmitting] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const authorize = (formState) => {
    router
      .open(
        `https://accounts.google.com/o/oauth2/auth?access_type=offline&client_id=${formState.clientId}&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&response_type=code&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fandroidpublisher`
      )
      .then(() => setCredentials(formState));
  };

  const connect = (formState) => {
    setSubmitting(true);
    setError(null);
    invoke("playConnect", {
      ...formState,
      ...credentials,
    })
      .then((r: any) => {
        if (r.error) throw new Error(r.error);
        finish();
      })
      .catch((e) => setError(e.message))
      .then(() => setSubmitting(false));
  };

  const submit = (formState) => {
    if (credentials) connect(formState);
    else authorize(formState);
  };

  return (
    <Form onSubmit={submit}>
      {({ formProps }: any) => (
        <form {...formProps}>
          {error && (
            <div className={styles.error}>
              <SectionMessage title="Something went wrong" appearance="error">
                <p>{error}</p>
              </SectionMessage>
            </div>
          )}
          {credentials ? (
            <>
              <img className={styles.guide__img} src={playCodeGuide} />
              <Field label="Client Code" name="code" isRequired>
                {({ fieldProps }: any) => (
                  <Fragment>
                    <Textfield
                      placeholder="4/1AX4XfWgnamRjpcNOBTQ7TWQkhi5S4wCbc0Tl5TId_lbpkW_LsjLst9gArO8"
                      {...fieldProps}
                    />
                  </Fragment>
                )}
              </Field>
            </>
          ) : (
            <>
              <div className={styles.guide__section}>
                <img className={styles.guide__img} src={projectGuide} />
                <h5>1. Link a Google Cloud project</h5>
                <p className={styles["guide__content--top"]}>
                  You can start by going to your{" "}
                  <a
                    href="https://play.google.com/console/api-access"
                    onClick={(event) => {
                      event.preventDefault();
                      router.open((event.target as HTMLAnchorElement).href);
                    }}
                  >
                    API access
                  </a>{" "}
                  settings, linking a Google Cloud project and configuring an
                  OAuth consent screen with the Google Play Android Developer
                  API scope. Please note that configuring Google Play Console
                  requires owner access to the developer account.
                </p>
              </div>
              <div className={styles.guide__section}>
                <img className={styles.guide__img} src={oauthGuide} />
                <h5>2. Create a new OAuth client</h5>
                <p className={styles.guide__content}>
                  You'll need to choose desktop as the application type and give
                  your OAuth client a name.
                </p>
              </div>
              <div className={styles.guide__section}>
                <img className={styles.guide__img} src={credentialsGuide} />
                <h5>2. Enter your OAuth client credentials</h5>
                <Field label="Your Client ID" name="clientId" isRequired>
                  {({ fieldProps }: any) => (
                    <Fragment>
                      <Textfield
                        placeholder="12345-XXX.apps.googleusercontent.com"
                        {...fieldProps}
                      />
                    </Fragment>
                  )}
                </Field>
                <Field
                  label="Your Client Secret"
                  name="clientSecret"
                  isRequired
                >
                  {({ fieldProps }: any) => (
                    <Fragment>
                      <Textfield
                        placeholder="FIlSUPRAYfF3uKJ6kJ94jw69"
                        {...fieldProps}
                      />
                    </Fragment>
                  )}
                </Field>
              </div>
            </>
          )}
          <Footer back={back}>
            {credentials ? (
              <LoadingButton
                type="submit"
                iconAfter={
                  <ArrowRightIcon label="Continue icon" size="small" />
                }
                appearance="primary"
                isLoading={submitting}
              >
                Connect
              </LoadingButton>
            ) : (
              <LoadingButton
                type="submit"
                iconAfter={<OpenIcon label="Open icon" size="small" />}
                appearance="primary"
              >
                Authorize
              </LoadingButton>
            )}
          </Footer>
        </form>
      )}
    </Form>
  );
}

function New() {
  const [selection, setSelection] = useState<string | null>(null);
  const [platform, setPlatform] = useState<string | null>(null);
  const [finished, setFinished] = useState(false);
  const animationDir = useRef(1);

  const back = () => setPlatform(null);

  const finish = () => {
    animationDir.current = -1;
    setFinished(true);
  };

  return (
    <Fragment>
      <div className={styles.progress__container}>
        <ProgressTracker
          items={[
            {
              id: "select",
              label: "Select platform",
              percentageComplete: platform ? 100 : 0,
              status: platform ? "visited" : "current",
              onClick: finished ? undefined : () => setPlatform(null),
            },
            {
              id: "connect",
              label: "Connect account",
              percentageComplete: finished ? 100 : 0,
              status: platform
                ? finished
                  ? "visited"
                  : "current"
                : "unvisited",
            },
            {
              id: "done",
              label: "Done!",
              percentageComplete: 0,
              status: finished ? "current" : "unvisited",
            },
          ]}
        />
      </div>
      <div className={styles.form__container}>
        <AnimatePresence exitBeforeEnter initial={false}>
          {!platform && (
            <motion.div
              transition={{ ease: "linear", duration: 0.15 }}
              custom={(value) => -1 * value}
              key="join"
              variants={animations}
              animate="animate"
              exit="exit"
              initial="initial"
            >
              <Form onSubmit={() => setPlatform(selection)}>
                {({ formProps }: any) => (
                  <form {...formProps}>
                    <div>
                      {Object.keys(Platforms).map((key) => (
                        <div
                          key={key}
                          className={
                            selection == key
                              ? styles["platform-selected"]
                              : styles.platform
                          }
                          onClick={() => setSelection(key)}
                        >
                          <img
                            className={styles.platform__logo}
                            src={Platforms[key].icon}
                          />
                          <div className={styles.platform__name}>
                            {Platforms[key].name}
                          </div>
                        </div>
                      ))}
                    </div>
                    <div className={styles.form__buttons}>
                      <Link to="/" unstyled>
                        <Button
                          iconBefore={
                            <ArrowLeftIcon label="Back icon" size="small" />
                          }
                        >
                          Cancel
                        </Button>
                      </Link>
                      <Button
                        type="submit"
                        iconAfter={
                          <ArrowRightIcon label="Continue icon" size="small" />
                        }
                        appearance="primary"
                        isDisabled={!selection}
                      >
                        Continue
                      </Button>
                    </div>
                  </form>
                )}
              </Form>
            </motion.div>
          )}
          {platform && !finished && (
            <motion.div
              transition={{ ease: "linear", duration: 0.15 }}
              custom={(value) => animationDir.current * value}
              key="connect"
              variants={animations}
              animate="animate"
              exit="exit"
              initial="initial"
            >
              {platform === "appfollow" && (
                <Appfollow back={back} finish={finish} />
              )}
              {platform === "appfigures" && (
                <Appfigures back={back} finish={finish} />
              )}
              {platform === "play" && <Play back={back} finish={finish} />}
            </motion.div>
          )}
          {platform && finished && (
            <motion.div
              transition={{ ease: "linear", duration: 0.15 }}
              custom={(value) => value}
              key="done"
              variants={animations}
              animate="animate"
              exit="exit"
              initial="initial"
            >
              <div className={styles.done_container}>
                <img src={DoneEmoji} className={styles.done_icon} />
                <h3>You just connected {Platforms[platform].name}!</h3>
                <p>
                  Well done! You can now manage reviews and link them with your
                  product roadmap.
                </p>
                <Link to="/" unstyled>
                  <Button
                    className={styles.done_button}
                    iconAfter={
                      <ArrowRightIcon label="Continue icon" size="small" />
                    }
                    appearance="primary"
                  >
                    Your connected platforms
                  </Button>
                </Link>
              </div>
            </motion.div>
          )}
        </AnimatePresence>
      </div>
    </Fragment>
  );
}

export default New;
