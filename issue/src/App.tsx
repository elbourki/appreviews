import React, { useContext, useEffect, useRef, useState } from "react";
import { invoke, Modal, view } from "@forge/bridge";
import {
  ApolloClient,
  ApolloProvider,
  gql,
  NormalizedCacheObject,
  useSubscription,
} from "@apollo/client";
import {
  createClient,
  Error,
  Loading,
  Review,
  TokenContext,
} from "@app-reviews/shared";
import { __RouterContext } from "react-router";
import styles from "./App.module.scss";
import Button, { LoadingButton } from "@atlaskit/button";
import { FullContext } from "@forge/bridge/out/types";

function Reviews({
  context: {
    extension: {
      issue: { key },
    },
  },
}) {
  const token = useContext(TokenContext);
  const { loading, error, data } = useSubscription(
    gql`
      subscription ($issueKeys: jsonb) {
        reviews(
          where: { issueKeys: { _contains: $issueKeys } }
          order_by: { createdAt: asc }
        ) {
          id
          appId
          author
          meta
          platformId
          reply
          review
        }
      }
    `,
    {
      variables: {
        issueKeys: [key],
      },
    }
  );

  const reply = () => {
    const modal = new Modal({
      resource: "reply-modal",
      size: "medium",
      context: {
        token,
        reviews: data.reviews,
      },
    });
    modal.open();
  };

  if (error) return <Error />;

  if (loading) return <Loading />;

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <h5>Linked reviews ({data.reviews.length})</h5>
        <Button onClick={reply} isDisabled={data.reviews.length === 0}>
          Reply to all
        </Button>
      </div>
      <div className={styles.list}>
        {data.reviews.map((review) => (
          <Review key={review.id} review={review} token={token} />
        ))}
      </div>
    </div>
  );
}

function App() {
  const [client, setClient] =
    useState<ApolloClient<NormalizedCacheObject> | null>(null);
  const [context, setContext] = useState<FullContext | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const tokenRef = useRef(null);

  useEffect(() => {
    view
      .getContext()
      .then((context) => {
        setContext(context);

        return invoke("generateGraphQLToken")
          .then((token: any) => (tokenRef.current = token))
          .then((token) => setClient(createClient(token)));
      })
      .catch(() => setError(true))
      .then(() => setLoading(false));
  }, []);

  if (error) return <Error />;

  if (loading) return <Loading />;

  return (
    <ApolloProvider client={client as any}>
      <TokenContext.Provider value={tokenRef.current}>
        <Reviews context={context as any} />
      </TokenContext.Provider>
    </ApolloProvider>
  );
}

export default App;
