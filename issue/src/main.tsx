import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "@atlaskit/css-reset";
import "./globals.scss";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
