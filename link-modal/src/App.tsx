import React, { useEffect, useState } from "react";
import { requestJira, view } from "@forge/bridge";
import {
  ApolloClient,
  ApolloProvider,
  gql,
  NormalizedCacheObject,
  useApolloClient,
  useMutation,
} from "@apollo/client";
import { createClient, Error, Loading } from "@app-reviews/shared";
import { __RouterContext } from "react-router";
import { AsyncSelect } from "@atlaskit/select";
import styles from "./App.module.scss";
import Button, { LoadingButton } from "@atlaskit/button";
import { FullContext } from "@forge/bridge/out/types";

function Link({
  context: {
    extension: {
      modal: { reviews },
      project: { id },
    },
  },
}) {
  const client = useApolloClient();
  const [url, setUrl] = useState(null);
  const [fetching, setFetching] = useState(true);
  const [issueKeys, setIssueKeys] = useState<string[]>([]);
  const [insert, { loading }] = useMutation(
    gql`
      mutation ($reviews: [reviews_insert_input!]!) {
        insert_reviews(
          objects: $reviews
          on_conflict: {
            constraint: reviews_appId_platformId_key
            update_columns: [issueKeys]
          }
        ) {
          affected_rows
        }
      }
    `
  );

  useEffect(() => {
    requestJira("/rest/api/3/serverInfo")
      .then((r) => r.json())
      .then(({ baseUrl }) => setUrl(baseUrl));
    if (reviews.length === 1) {
      client
        .query({
          query: gql`
            query ($platformId: String!, $appId: uuid!) {
              reviews(
                where: {
                  platformId: { _eq: $platformId }
                  appId: { _eq: $appId }
                }
              ) {
                issueKeys
              }
            }
          `,
          variables: {
            platformId: reviews[0].platformId,
            appId: reviews[0].appId,
          },
        })
        .then(({ data: { reviews } }) =>
          setIssueKeys(reviews[0]?.issueKeys || [])
        )
        .then(() => setFetching(false));
    } else {
      setFetching(false);
    }
  }, []);

  const load = async (inputValue: string) => {
    return requestJira(
      `/rest/api/3/issue/picker?currentProjectId=${id}&currentJQL=${encodeURIComponent(
        "ORDER BY updated"
      )}&query=${encodeURIComponent(inputValue)}`
    )
      .then((r) => r.json())
      .then(
        (r) =>
          r.sections.find(({ id }) => id === (inputValue ? "cs" : "hs"))
            ?.issues || []
      )
      .then((issues) =>
        issues.map((issue) => ({
          label: issue.key,
          value: issue.key,
          issue: {
            img: issue.img,
            summary: issue.summaryText,
          },
        }))
      );
  };

  const link = () => {
    return insert({
      variables: {
        reviews: reviews.map((review) => ({
          ...review,
          id: undefined,
          __typename: undefined,
          issueKeys,
        })),
      },
    }).then(() => view.close());
  };

  return (
    <div className={styles.container}>
      <h3>Link with your roadmap</h3>
      <div className={styles.content}>
        <AsyncSelect
          isMulti
          placeholder="Choose an issue"
          cacheOptions
          defaultOptions
          loadOptions={load}
          value={issueKeys.map((key) => ({
            label: key,
            value: key,
          }))}
          onChange={(issues) =>
            setIssueKeys(issues.map((issue) => issue.value as string))
          }
          components={{
            Option: ({ innerProps, data, isFocused }) => (
              <div
                className={
                  isFocused ? styles["option--focused"] : styles.option
                }
                {...innerProps}
              >
                {url && <img src={`${url}/${data.issue.img}`} />}
                {data.label} - {data.issue.summary}
              </div>
            ),
          }}
        />
      </div>
      <div className={styles.buttons}>
        <Button onClick={() => view.close()} appearance="subtle">
          Cancel
        </Button>
        <LoadingButton
          appearance="primary"
          onClick={link}
          isLoading={loading || fetching}
        >
          Save
        </LoadingButton>
      </div>
    </div>
  );
}

function App() {
  const [client, setClient] =
    useState<ApolloClient<NormalizedCacheObject> | null>(null);
  const [context, setContext] = useState<FullContext | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    view
      .getContext()
      .then((context) => {
        setContext(context);
        setClient(createClient((context as any).extension.modal.token));
      })
      .catch(() => setError(true))
      .then(() => setLoading(false));
  }, []);

  return (
    <>
      {error && <Error />}
      {!error && loading && <Loading />}
      {context && client && (
        <ApolloProvider client={client}>
          <Link context={context as any} />
        </ApolloProvider>
      )}
    </>
  );
}

export default App;
