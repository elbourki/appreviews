import React, { useEffect, useRef, useState } from "react";
import { invoke, view } from "@forge/bridge";
import {
  ApolloClient,
  ApolloProvider,
  NormalizedCacheObject,
} from "@apollo/client";
import Manage from "./pages/Manage";
import {
  createClient,
  Error,
  Loading,
  TokenContext,
} from "@app-reviews/shared";
import { Router, Route, Switch, __RouterContext } from "react-router";

function App() {
  const [client, setClient] =
    useState<ApolloClient<NormalizedCacheObject> | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [history, setHistory] = useState<any>(null);
  const tokenRef = useRef(null);

  useEffect(() => {
    view.createHistory().then((newHistory) => setHistory(newHistory));

    invoke("generateGraphQLToken")
      .then((token: any) => (tokenRef.current = token))
      .then((token) => setClient(createClient(token)))
      .catch(() => setError(true))
      .then(() => setLoading(false));
  }, []);

  return (
    <>
      {error && <Error />}
      {!error && (loading || !history) && <Loading />}
      {client && (
        <ApolloProvider client={client}>
          <TokenContext.Provider value={tokenRef.current}>
            {history && (
              <Router history={history}>
                <Switch>
                  <Route path="/">
                    <Manage />
                  </Route>
                </Switch>
              </Router>
            )}
          </TokenContext.Provider>
        </ApolloProvider>
      )}
    </>
  );
}

export default App;
