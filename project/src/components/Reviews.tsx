import React, { useEffect, useState } from "react";
import styles from "./Reviews.module.scss";
import Pagination from "@atlaskit/pagination";
import Textfield from "@atlaskit/textfield";
import Button from "@atlaskit/button";
import Form, { Field, FormFooter } from "@atlaskit/form";
import Switcher from "../components/Switcher";
import Spinner from "@atlaskit/spinner";
import { invoke } from "@forge/bridge";
import {
  EmptyState,
  Platforms,
  Review,
  TokenContext,
} from "@app-reviews/shared";
import Select from "@atlaskit/select";
import { useContext } from "react";
import { Error } from "@app-reviews/shared";

const languages = [
  {
    label: "None",
    value: "none",
  },
  {
    label: "English",
    value: "en",
  },
  {
    label: "Arabic",
    value: "ar",
  },
  {
    label: "Chinese",
    value: "zh-cn",
  },
  {
    label: "Dutch",
    value: "nl",
  },
  {
    label: "French",
    value: "fr",
  },
  {
    label: "German",
    value: "de",
  },
  {
    label: "Hebrew",
    value: "he",
  },
  {
    label: "Hindi",
    value: "hi",
  },
  {
    label: "Italian",
    value: "it",
  },
  {
    label: "Japanese",
    value: "ja",
  },
  {
    label: "Korean",
    value: "ko",
  },
  {
    label: "Norwegian",
    value: "no",
  },
  {
    label: "Polish",
    value: "pl",
  },
  {
    label: "Portuguese",
    value: "pt",
  },
  {
    label: "Russian",
    value: "ru",
  },
  {
    label: "Spanish",
    value: "es",
  },
  {
    label: "Swedish",
    value: "sv",
  },
  {
    label: "Turkish",
    value: "tr",
  },
  {
    label: "Vietnamese",
    value: "vi",
  },
];

function Reviews({ apps }) {
  const [reviews, setReviews] = useState<any[]>([]);
  const [selected, setSelected] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [pagination, setPagination] = useState<any>(null);
  const [page, setPage] = useState(1);
  const [cursor, setCursor] = useState(null);
  const [language, setLanguage] = useState("none");
  const [search, setSearch] = useState("");
  const token = useContext(TokenContext);

  const app = apps.find(({ id }) => id === selected) || apps[0];

  const fetch = () => {
    setError(false);
    setLoading(true);
    invoke(`${app.connection.platform}Reviews`, {
      id: app.id,
      page,
      language,
      cursor,
      search,
    })
      .then((response: any) => {
        setReviews(response.reviews);
        setPagination(response.pagination);
      })
      .catch(() => setError(true))
      .finally(() => setLoading(false));
  };

  const reset = () => {
    setCursor(null);
    setPage(1);
  };

  const filter = (formState) => {
    setLanguage(formState.language?.value);
    setSearch(formState.search);
    reset();
  };

  useEffect(() => {
    fetch();
  }, [selected, apps, cursor, page, search, language]);

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [page, cursor]);

  return (
    <div className={styles.reviews__container}>
      <div className={styles.reviews__sidebar}>
        <Switcher
          apps={apps}
          app={app}
          onChange={({ value }) => setSelected(value)}
        />
        <Form onSubmit={filter}>
          {({ formProps }: any) => (
            <form {...formProps}>
              {["play", "appfigures"].includes(app.connection.platform) && (
                <Field
                  label="Translate into"
                  name="language"
                  defaultValue={languages.find((l) => l.value === language)}
                >
                  {({ fieldProps }: any) => (
                    <Select
                      {...fieldProps}
                      options={languages}
                      placeholder="Choose a language"
                    />
                  )}
                </Field>
              )}
              {["appfollow", "appfigures"].includes(
                app.connection.platform
              ) && (
                <Field label="Search" name="search">
                  {({ fieldProps }: any) => (
                    <Textfield
                      placeholder="Enter your search query"
                      {...fieldProps}
                    />
                  )}
                </Field>
              )}
              <FormFooter>
                <Button type="submit" appearance="primary">
                  Submit
                </Button>
              </FormFooter>
            </form>
          )}
        </Form>
      </div>
      <div className={styles.reviews}>
        {loading && (
          <div className={styles.reviews__loading}>
            <Spinner size="medium" />
            <h5>
              Fetching reviews from {Platforms[app.connection.platform].name}...
            </h5>
          </div>
        )}
        {error ? (
          <Error>
            <p>Couldn't load your reviews. Possible reasons:</p>
            <ul className={styles.reasons}>
              <li>You have exceeded your API request quota</li>
              <li>Your account is no longer connected</li>
              <li>You no longer have access to the app</li>
              <li>The request has timed out</li>
            </ul>
          </Error>
        ) : reviews.length ? (
          <>
            <div className={styles.reviews__list}>
              {reviews.map((review) => (
                <Review
                  key={review.platformId}
                  review={{ ...review, appId: app.id }}
                  token={token}
                />
              ))}
            </div>
            {pagination && pagination.type === "cursor" && (
              <div className={styles.pagination__cursor}>
                <Button
                  onClick={() => setCursor(pagination.previous)}
                  isDisabled={!pagination.previous}
                >
                  Previous
                </Button>
                <Button
                  onClick={() => setCursor(pagination.next)}
                  isDisabled={!pagination.next}
                >
                  Next
                </Button>
              </div>
            )}
            {pagination && pagination.type === "pages" && (
              <div className={styles.pagination}>
                <Pagination
                  onChange={(_e, page) => setPage(page)}
                  selectedIndex={page - 1}
                  pages={Array.from(
                    new Array(pagination.total),
                    (x, i) => i + 1
                  )}
                />
              </div>
            )}
          </>
        ) : (
          !loading && (
            <EmptyState>
              <h3>No results found</h3>
              <p>Try refining your search or check back later.</p>
            </EmptyState>
          )
        )}
      </div>
    </div>
  );
}

export default Reviews;
