import { PopupSelect } from "@atlaskit/select";
import React from "react";
import styles from "./Switcher.module.scss";
import { Stores } from "@app-reviews/shared";

function Switcher({ apps, app, onChange }) {
  return (
    <PopupSelect
      onChange={onChange}
      value={{
        value: app.id,
      }}
      options={apps.map((app) => ({
        label: app.name,
        value: app.id,
        app: app,
      }))}
      components={{
        Option: ({ data: { app }, innerProps, isSelected }) => (
          <div
            {...innerProps}
            className={isSelected ? styles["app--selected"] : styles.app}
          >
            <div className={styles.icon__container}>
              <img className={styles.icon} src={app.icon} />
            </div>
            <div>
              <h5>{app.name}</h5>
              <small>
                {[Stores[app.store].name, app.developer]
                  .filter(Boolean)
                  .join(" • ")}
              </small>
            </div>
          </div>
        ),
      }}
      target={({ isOpen, ...triggerProps }) => (
        <div
          {...triggerProps}
          className={isOpen ? styles["app-box--open"] : styles["app-box"]}
        >
          <div className={styles.icon__container}>
            <img className={styles.icon} src={app.icon} />
          </div>
          <div>
            <h5>{app.name}</h5>
            <small>
              {[Stores[app.store].name, app.developer]
                .filter(Boolean)
                .join(" • ")}
            </small>
          </div>
        </div>
      )}
    />
  );
}

export default Switcher;
