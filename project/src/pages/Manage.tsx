import { gql, useSubscription } from "@apollo/client";
import React, { Fragment } from "react";
import { Loading, Error, EmptyState } from "@app-reviews/shared";
import PageHeader from "@atlaskit/page-header";
import Reviews from "../components/Reviews";

function Manage() {
  const { loading, error, data } = useSubscription(
    gql`
      subscription {
        apps(order_by: { createdAt: asc }) {
          id
          name
          developer
          icon
          store
          connection {
            platform
          }
        }
      }
    `
  );

  return (
    <Fragment>
      <PageHeader>Manage reviews</PageHeader>
      {error && <Error />}
      {loading && <Loading />}
      {!loading &&
        !error &&
        (data.apps.length ? (
          <Reviews apps={data.apps} />
        ) : (
          <EmptyState>
            <h3>This project isn't linked to any apps</h3>
            <p>
              Ask your admin to link this project's apps. If you're an admin,
              head over to the settings to get started!
            </p>
          </EmptyState>
        ))}
    </Fragment>
  );
}

export default Manage;
