import React, { useEffect, useState } from "react";
import { invoke, Modal, view } from "@forge/bridge";
import {
  ApolloClient,
  ApolloProvider,
  gql,
  NormalizedCacheObject,
  useMutation,
  useSubscription,
} from "@apollo/client";
import {
  createClient,
  Error as ErrorComponent,
  Loading,
  OkEmoji,
  Platforms,
  Rating,
  WarningEmoji,
} from "@app-reviews/shared";
import { __RouterContext } from "react-router";
import styles from "./App.module.scss";
import Button, { LoadingButton } from "@atlaskit/button";
import { FullContext } from "@forge/bridge/out/types";
import Form, {
  ErrorMessage,
  Field,
  FormFooter,
  HelperMessage,
} from "@atlaskit/form";
import TextArea from "@atlaskit/textarea";
import pluralize from "pluralize";
import ProgressBar from "@atlaskit/progress-bar";
import Lozenge from "@atlaskit/lozenge";
import { AnimatePresence, motion } from "framer-motion";
import { Code } from "@atlaskit/code";
import Select from "@atlaskit/select";

const animations = {
  animate: { opacity: 1, x: 0, y: 0 },
  exit: {
    opacity: 0,
    x: 20,
    y: 0,
  },
  initial: {
    opacity: 0,
    x: 20,
    y: 0,
  },
};

function Reply({
  context: {
    extension: {
      modal: { token, reviews },
    },
  },
}) {
  const [done, setDone] = useState<number | null>(null);
  const [replying, setReplying] = useState(false);
  const [progress, setProgress] = useState(0);
  const { data: templatesData, loading: loadingTemplates } = useSubscription(
    gql`
      subscription {
        templates {
          id
          name
          template
        }
      }
    `
  );
  const [insert, { loading, data }] = useMutation(
    gql`
      mutation ($reviews: [reviews_insert_input!]!) {
        insert_reviews(
          objects: $reviews
          on_conflict: {
            constraint: reviews_appId_platformId_key
            update_columns: review
          }
        ) {
          affected_rows
          returning {
            app {
              connection {
                platform
              }
              icon
              name
              store
            }
            id
            author
            review(path: "stars")
          }
        }
      }
    `
  );

  const templates = (templatesData?.templates || []).map((template) => ({
    label: template.name,
    value: template.id,
    template: template.template,
  }));

  const fetched = data?.insert_reviews?.returning || [];
  const current = fetched[progress];

  useEffect(() => {
    insert({
      variables: {
        reviews: reviews.map((review) => ({
          ...review,
          id: undefined,
          __typename: undefined,
          issueKeys: [],
        })),
      },
    });
  }, []);

  const reply = async ({ reply }) => {
    setReplying(true);
    let sent = 0;
    for (let i = 0; i < fetched.length; i++) {
      setProgress(i);
      const review = fetched[i];
      await invoke(`${review.app.connection.platform}Reply`, {
        id: review.id,
        reply,
      })
        .then((r: any) => {
          if (r.error) throw new Error(r.error);
        })
        .then(() => {
          sent++;
        })
        .catch(console.error);
    }
    setDone(sent);
  };

  const manageTemplates = () => {
    const modal = new Modal({
      resource: "templates-modal",
      size: "medium",
      context: {
        token,
      },
    });
    modal.open();
  };

  if (done !== null)
    return (
      <motion.div
        transition={{ ease: "linear", duration: 0.15 }}
        key="done"
        variants={animations}
        animate="animate"
        exit="exit"
        initial="initial"
        className={styles.done}
      >
        <img
          src={done < fetched.length ? WarningEmoji : OkEmoji}
          className={styles.done__icon}
        />
        <h3>
          {done === 0
            ? `We couldn't send your ${pluralize("reply", fetched.length)}.`
            : `We have sent ${fetched.length} ${pluralize(
                "reply",
                fetched.length
              )} successfully!`}
        </h3>
        {done < fetched.length && (
          <>
            <p>
              {fetched.length - done}{" "}
              {pluralize("request", fetched.length - done)} failed. Possible
              reasons:
            </p>
            <ul className={styles.reasons}>
              <li>You have exceeded your API request quota</li>
              <li>Your account is no longer connected</li>
              <li>The review was deleted by the author</li>
              <li>Your plan doesn't allow replying to reviews</li>
            </ul>
          </>
        )}
        <Button
          className={styles.done__button}
          onClick={() => view.close()}
          appearance="primary"
        >
          Close modal
        </Button>
      </motion.div>
    );

  if (replying)
    return (
      <motion.div
        transition={{ ease: "linear", duration: 0.15 }}
        key="replying"
        variants={animations}
        animate="animate"
        exit="exit"
        initial="initial"
        className={styles.replying}
      >
        <div className={styles.box}>
          <div className={styles.icon__container}>
            <img className={styles.icon} src={current.app.icon} />
          </div>
          <div className={styles.content}>
            <h5>{current.author}</h5>
            <Rating stars={current.review} size="small" />
          </div>
        </div>
        <div className={styles.status}>
          <Lozenge>
            {progress + 1}/{fetched.length}
          </Lozenge>{" "}
          Replying to <b>{current.author}</b> via{" "}
          <b>{Platforms[current.app.connection.platform].name}</b>...
        </div>
        <ProgressBar value={progress / fetched.length} />
      </motion.div>
    );

  return (
    <motion.div
      transition={{ ease: "linear", duration: 0 }}
      key="reply"
      variants={animations}
      animate="animate"
      exit="exit"
      initial="initial"
      className={styles.container}
    >
      <div className={styles.header}>
        <h3>
          Replying to {reviews.length} {pluralize("review", reviews.length)}
        </h3>
        <Button onClick={() => manageTemplates()}>Manage templates</Button>
      </div>
      <Form onSubmit={reply}>
        {({ formProps, setFieldValue }) => (
          <form {...formProps}>
            <Select
              placeholder="Insert a template"
              onChange={({ template }: any) => setFieldValue("reply", template)}
              options={templates}
              value={null}
            />
            <Field
              label="Your reply"
              isRequired
              name="reply"
              validate={(value: any) => {
                if (value.length > 350) return "TOO_LONG";
              }}
              defaultValue=""
            >
              {({ fieldProps, error }: any) => (
                <>
                  <TextArea {...fieldProps} minimumRows={3} />
                  {error === "TOO_LONG" && (
                    <ErrorMessage>
                      Your reply can only contain 350 characters at most.
                    </ErrorMessage>
                  )}
                  <HelperMessage>
                    You can mix variables, conditions and filters to send a
                    personalized reply!
                    <br />
                    Here are some examples to get you started:
                  </HelperMessage>
                  <HelperMessage>
                    <ul>
                      <li>
                        <Code>{`{{ author | split: " " | first | capitalize }}`}</Code>
                      </li>
                      <li>
                        <Code>{`{{ timestamp | date: "%a, %b %d, %y" }}`}</Code>
                      </li>
                      <li>
                        <Code>{`{% if stars < 3 %}Would you please consider updating your rating?{% endif %}`}</Code>
                      </li>
                    </ul>
                  </HelperMessage>
                </>
              )}
            </Field>
            <FormFooter>
              <Button onClick={() => view.close()} appearance="subtle">
                Cancel
              </Button>
              <LoadingButton
                type="submit"
                appearance="primary"
                isLoading={loading || loadingTemplates}
              >
                Submit
              </LoadingButton>
            </FormFooter>
          </form>
        )}
      </Form>
    </motion.div>
  );
}

function App() {
  const [client, setClient] =
    useState<ApolloClient<NormalizedCacheObject> | null>(null);
  const [context, setContext] = useState<FullContext | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    view
      .getContext()
      .then((context) => {
        setContext(context);
        setClient(createClient((context as any).extension.modal.token));
      })
      .catch(() => setError(true))
      .then(() => setLoading(false));
  }, []);

  if (error) return <ErrorComponent />;

  if (loading) return <Loading />;

  return (
    <ApolloProvider client={client as any}>
      <AnimatePresence exitBeforeEnter initial={true}>
        <Reply context={context as any} />
      </AnimatePresence>
    </ApolloProvider>
  );
}

export default App;
