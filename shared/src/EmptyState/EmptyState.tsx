import React from "react";
import styles from "./EmptyState.module.scss";
import EmptyStateIcon from "../../assets/empty-state.svg";

function EmptyState({ children }: any) {
  return (
    <div className={styles.container}>
      <img src={EmptyStateIcon} className={styles.icon} />
      {children}
    </div>
  );
}

export default EmptyState;
