import React from "react";
import styles from "./Error.module.scss";
import ErrorIcon from "../../assets/error.svg";

type Props = {
  children?: any;
};

function Error({ children }: Props) {
  return (
    <div className={styles.container}>
      <img src={ErrorIcon} className={styles.icon} />
      <h3>Something went wrong</h3>
      {children || (
        <p>
          Please check to make sure you have sufficient permissions or try again
          later. If the problem persists please contact us.
        </p>
      )}
    </div>
  );
}

export default Error;
