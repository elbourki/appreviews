import { ApolloClient, InMemoryCache } from "@apollo/client";
import { WebSocketLink } from "@apollo/client/link/ws";

export const createClient = (token, headers = {}) =>
  new ApolloClient({
    link: new WebSocketLink({
      uri: "wss://reviews.hasura.app/v1/graphql",
      options: {
        reconnect: true,
        connectionParams: {
          headers: {
            Authorization: `Bearer ${token}`,
            ...headers,
          },
        },
      },
    }),
    cache: new InMemoryCache(),
  });
