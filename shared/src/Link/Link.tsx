import React, { useContext } from "react";
import { __RouterContext } from "react-router";
import styles from "./Link.module.scss";

function Link({ to, children, unstyled }: any) {
  const { history } = useContext(__RouterContext);

  return (
    <a
      href={to}
      onClick={(event) => {
        event.preventDefault();
        history.push(to);
      }}
      className={unstyled && styles.link}
    >
      {children}
    </a>
  );
}

export default Link;
