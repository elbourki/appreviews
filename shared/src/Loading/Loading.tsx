import Spinner from "@atlaskit/spinner";
import React from "react";
import styles from "./Loading.module.scss";

function Loading() {
  return (
    <div className={styles.container}>
      <Spinner size="large" />
    </div>
  );
}

export default Loading;
