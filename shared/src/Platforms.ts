import PlayLogo from "../assets/platforms/play.png";
import appfiguresLogo from "../assets/platforms/appfigures.png";
import appFollowLogo from "../assets/platforms/appfollow.png";

export default {
  play: {
    name: "Google Play Console",
    icon: PlayLogo,
  },
  appfigures: {
    name: "Appfigures",
    icon: appfiguresLogo,
    syncable: true,
  },
  appfollow: {
    name: "AppFollow",
    icon: appFollowLogo,
    syncable: true,
  },
};
