import React from "react";
import styles from "./Rating.module.scss";
import StarFilledIcon from "@atlaskit/icon/glyph/star-filled";

function Rating({ stars, size }: any) {
  return (
    <div
      className={size === "small" ? styles["ratings--small"] : styles.ratings}
    >
      {[...new Array(stars)].map((_v, k) => (
        <StarFilledIcon
          size={size}
          label="Star Filled"
          primaryColor="#fba417"
          key={k}
        />
      ))}
      {[...new Array(5 - stars)].map((_v, k) => (
        <StarFilledIcon
          size={size}
          label="Star Unfilled"
          primaryColor="#d6d3cf"
          key={k}
        />
      ))}
    </div>
  );
}

export default Rating;
