import React from "react";
import { Rating } from "..";
import styles from "./Review.module.scss";
import Tooltip from "@atlaskit/tooltip";
import { useState } from "react";
import Button from "@atlaskit/button";
import { Modal } from "@forge/bridge";

function Review({ review, token }) {
  const [original, setOriginal] = useState(false);

  const link = () => {
    const modal = new Modal({
      resource: "link-modal",
      size: "small",
      context: {
        token,
        reviews: [review],
      },
    });
    modal.open();
  };

  const reply = () => {
    const modal = new Modal({
      resource: "reply-modal",
      size: "medium",
      context: {
        token,
        reviews: [review],
      },
    });
    modal.open();
  };

  return (
    <div className={styles.review}>
      <div className={styles.review__header}>
        <Rating stars={review.review.stars} />
        <Tooltip
          content={new Intl.DateTimeFormat("en-US", {
            dateStyle: "full",
            timeStyle: "short",
          }).format(new Date(review.review.timestamp * 1000))}
          tag="small"
        >
          {new Intl.DateTimeFormat("en-US", {
            dateStyle: "full",
          }).format(new Date(review.review.timestamp * 1000))}
        </Tooltip>
      </div>
      {review.review.title && (
        <p className={styles.review__title}>
          {original ? review.review.originalTitle : review.review.title}
        </p>
      )}
      <p className={styles.review__content}>
        {original ? review.review.originalText : review.review.text}
      </p>
      {review.reply && (
        <div className={styles.review__reply}>
          <small>
            Replied on{" "}
            {new Intl.DateTimeFormat("en-US", {
              dateStyle: "long",
            }).format(new Date(review.reply.timestamp * 1000))}
            :
          </small>
          <p className={styles.review__reply__content}>{review.reply.text}</p>
        </div>
      )}
      <div className={styles.review__footer}>
        <small className={styles.review__details}>
          {[
            `By ${review.author}`,
            review.review.version && `Version: ${review.review.version}`,
          ]
            .filter(Boolean)
            .join(" • ")}
          {review.review.originalText && (
            <>
              {" "}
              •{" "}
              <a className={styles.link} onClick={() => setOriginal(!original)}>
                Show {original ? "translation" : "original"}
              </a>
            </>
          )}
        </small>
        <div className={styles.review__actions}>
          <Button onClick={link}>Linked issues</Button>
          <Button onClick={reply} appearance="primary">
            Reply
          </Button>
        </div>
      </div>
    </div>
  );
}

export default Review;
