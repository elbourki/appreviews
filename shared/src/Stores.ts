export default {
  amazon: {
    name: "Amazon Appstore",
  },
  microsoftstore: {
    name: "Microsoft Store",
  },
  macstore: {
    name: "Mac App Store",
  },
  itunes: {
    name: "App Store",
  },
  googleplay: {
    name: "Google Play Store",
  },
};
