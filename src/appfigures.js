import {
  is_admin,
  client,
  jsonify,
  connect,
  error,
  replyText,
  updateReply,
} from "./helpers";
import { gql } from "@apollo/client";
import { fetch, storage } from "@forge/api";
import queryString from "query-string";
import { languages } from "./data";

export const appfiguresAuthorize = async (req) => {
  if (!(await is_admin()))
    return error("You don't have the permission to add a connection.")();

  return fetch(
    `https://api.appfigures.com/v2/oauth/request_token?oauth_signature_method=PLAINTEXT&oauth_consumer_key=${encodeURIComponent(
      process.env.APPFIGURES_CLIENT_KEY
    )}&oauth_callback=oob&oauth_signature=${encodeURIComponent(
      process.env.APPFIGURES_SECRET_KEY + "&"
    )}`
  )
    .then((r) => r.text())
    .then((body) => queryString.parse(body))
    .then(({ oauth_token, oauth_token_secret }) =>
      client().mutate({
        mutation: gql`
          mutation InsertSecret($secret: String, $token: String) {
            insert_secrets_one(object: { secret: $secret, token: $token }) {
              token
            }
          }
        `,
        variables: {
          token: oauth_token,
          secret: oauth_token_secret,
        },
      })
    )
    .then(
      ({
        data: {
          insert_secrets_one: { token },
        },
      }) => ({
        url: `https://api.appfigures.com/v2/oauth/authorize?oauth_token=${token}`,
        token,
      })
    );
};

export const appfiguresConnect = async (req) => {
  if (!(await is_admin()))
    return error("You don't have the permission to add a connection.")();
  const { code, token } = req.payload;

  return client()
    .query({
      query: gql`
        query GetSecret($token: String!) {
          secrets_by_pk(token: $token) {
            secret
          }
        }
      `,
      variables: {
        token,
      },
    })
    .then(
      ({
        data: {
          secrets_by_pk: { secret },
        },
      }) =>
        fetch(
          `https://api.appfigures.com/v2/oauth/access_token?oauth_signature_method=PLAINTEXT&oauth_consumer_key=${encodeURIComponent(
            process.env.APPFIGURES_CLIENT_KEY
          )}&oauth_token=${encodeURIComponent(
            token
          )}&oauth_verifier=${encodeURIComponent(
            code
          )}&oauth_signature=${encodeURIComponent(
            process.env.APPFIGURES_SECRET_KEY + "&" + secret
          )}`
        )
          .then(jsonify)
          .then((body) => queryString.parse(body))
          .then(({ oauth_token, oauth_token_secret }) =>
            fetch("https://api.appfigures.com/v2/", {
              headers: {
                Authorization: `OAuth oauth_signature_method=PLAINTEXT, oauth_consumer_key=${process.env.APPFIGURES_CLIENT_KEY}, oauth_token=${oauth_token}, oauth_signature=${process.env.APPFIGURES_SECRET_KEY}&${oauth_token_secret}`,
              },
            })
              .then((r) => r.json())
              .then(({ user }) =>
                connect(
                  req.context,
                  "appfigures",
                  {
                    token: oauth_token,
                    secret: oauth_token_secret,
                  },
                  {
                    name: user.name,
                    email: user.email,
                  }
                )
              )
          )
    )
    .catch(
      error(
        "Unable to connect your Appfigures account, please verify the code you entered."
      )
    );
};

const mapping = {
  amazon_appstore: "amazon",
  apple: "itunes",
  google_play: "googleplay",
  windows10: "microsoftstore",
};

export const appfiguresSync = async (req) => {
  if (!(await is_admin()))
    return error("You don't have the permission to sync apps.")();
  const { id: connectionId } = req.payload;

  return storage.getSecret(`credentials#${connectionId}`).then((credentials) => {
    if (!credentials) return error("Can't find connection")();
    const { token, secret } = credentials;

    return fetch("https://api.appfigures.com/v2/products/mine", {
      headers: {
        Authorization: `OAuth oauth_signature_method=PLAINTEXT, oauth_consumer_key=${process.env.APPFIGURES_CLIENT_KEY}, oauth_token=${token}, oauth_signature=${process.env.APPFIGURES_SECRET_KEY}&${secret}`,
      },
    })
      .then(jsonify)
      .then((apps) =>
        Object.values(apps)
          .filter(
            (app) =>
              Object.keys(mapping).includes(app.store) &&
              app.source.hidden === false
          )
          .map((app) => ({
            connectionId,
            platformId: app.id + "",
            store: mapping[app.store],
            extId: app.bundle_identifier,
            name: app.name,
            developer: app.developer,
            icon: app.icon,
          }))
      )
      .then((apps) =>
        client()
          .mutate({
            mutation: gql`
              mutation InsertApps($apps: [apps_insert_input!]!) {
                insert_apps(
                  objects: $apps
                  on_conflict: {
                    constraint: apps_connectionId_extId_key
                    update_columns: [name, icon, developer]
                  }
                ) {
                  returning {
                    id
                  }
                }
              }
            `,
            variables: {
              apps,
            },
          })
          .then(({ data }) => data.insert_apps.returning)
      )
      .then((result) =>
        client().mutate({
          mutation: gql`
            mutation DeleteApps($connectionId: uuid!, $skip: [uuid!]) {
              delete_apps(
                where: {
                  _and: {
                    _not: { id: { _in: $skip } }
                    connectionId: { _eq: $connectionId }
                  }
                }
              ) {
                returning {
                  id
                }
              }
            }
          `,
          variables: {
            connectionId,
            skip: result.map((data) => data.id),
          },
        })
      )
      .catch(error("Couldn't sync apps"));
  });
};

const processReview = (review) => {
  const originalText =
    review.review === review.original_review ? null : review.original_review;
  const originalTitle =
    review.review === review.original_title ? null : review.original_title;
  return {
    platformId: review.id,
    author: review.author,
    review: {
      version: review.version,
      timestamp: new Date(review.date).getTime() / 1000,
      stars: Math.floor(parseFloat(review.stars)),
      originalText,
      text: review.review,
      originalTitle,
      title: review.title,
    },
    reply: null,
    meta: {},
  };
};

export const appfiguresReviews = async (req) => {
  const { id, search, language, page } = req.payload;
  const { cloudId } = req.context;

  const lang = languages.includes(language) ? language : undefined;

  return client()
    .query({
      query: gql`
        query ($id: uuid!) {
          apps_by_pk(id: $id) {
            connection {
              id
              cloudId
            }
            platformId
          }
        }
      `,
      variables: {
        id,
      },
    })
    .then(
      ({
        data: {
          apps_by_pk: { platformId, connection },
        },
      }) =>
        storage.getSecret(`credentials#${connection.id}`).then((credentials) => {
          if (!credentials || connection.cloudId !== cloudId)
            return error("Can't find connection")();

          const { token, secret } = credentials;

          return fetch(
            `https://api.appfigures.com/v2/reviews?${queryString.stringify({
              products: platformId,
              page,
              q: search,
              lang,
            })}`,
            {
              headers: {
                Authorization: `OAuth oauth_signature_method=PLAINTEXT, oauth_consumer_key=${process.env.APPFIGURES_CLIENT_KEY}, oauth_token=${token}, oauth_signature=${process.env.APPFIGURES_SECRET_KEY}&${secret}`,
              },
            }
          )
            .then(jsonify)
            .then((response) => ({
              reviews: response.reviews.map(processReview),
              pagination: {
                type: "pages",
                current: response.this_page,
                total: response.pages,
              },
            }));
        })
    );
};

export const appfiguresReply = async (req) => {
  const { id, reply } = req.payload;
  const { cloudId } = req.context;

  return client()
    .query({
      query: gql`
        query ($id: uuid!) {
          reviews_by_pk(id: $id) {
            app {
              connection {
                id
                cloudId
              }
            }
            platformId
            author
            review
          }
        }
      `,
      variables: {
        id,
      },
    })
    .then(
      ({
        data: {
          reviews_by_pk: {
            app: { connection },
            platformId,
            ...review
          },
        },
      }) =>
        storage
          .getSecret(`credentials#${connection.id}`)
          .then(async (credentials) => {
            if (!credentials || connection.cloudId !== cloudId)
              return error("Can't find connection")();

            const { token, secret } = credentials;

            const text = await replyText(reply, review);
            return fetch(
              `https://api.appfigures.com/v2/reviews/${platformId}/response`,
              {
                method: "POST",
                body: JSON.stringify({
                  content: text,
                }),
                headers: {
                  "Content-Type": "application/json",
                  Authorization: `OAuth oauth_signature_method=PLAINTEXT, oauth_consumer_key=${process.env.APPFIGURES_CLIENT_KEY}, oauth_token=${token}, oauth_signature=${process.env.APPFIGURES_SECRET_KEY}&${secret}`,
                },
              }
            )
              .then(jsonify)
              .then(() => updateReply(id, text));
          })
    );
};
