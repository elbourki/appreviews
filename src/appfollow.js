import {
  is_admin,
  client,
  jsonify,
  error,
  connect,
  replyText,
  updateReply,
} from "./helpers";
import { gql } from "@apollo/client";
import { fetch, storage } from "@forge/api";
import queryString from "query-string";

export const appfollowConnect = async (req) => {
  if (!(await is_admin()))
    return error("You don't have the permission to add a connection.")();

  return fetch(`https://api.appfollow.io/users`, {
    headers: {
      Authorization:
        "Basic " + Buffer.from(req.payload.apiSecret + ":").toString("base64"),
    },
  })
    .then(jsonify)
    .then((users) => {
      users.sort((a, b) => a.id - b.id);
      const user = users.filter(
        (user) => user.role === "admin" && user.status === "active"
      )[0];
      return connect(
        req.context,
        "appfollow",
        {
          apiSecret: req.payload.apiSecret,
        },
        {
          name: user.name,
          email: user.email,
        }
      );
    })
    .catch(
      error(
        "Unable to connect your AppFollow account, please verify the API secret you entered."
      )
    );
};

export const appfollowSync = async (req) => {
  if (!(await is_admin()))
    return error("You don't have the permission to sync apps.")();
  const { id: connectionId } = req.payload;

  return storage.getSecret(`credentials#${connectionId}`).then((credentials) => {
    if (!credentials) return error("Can't find connection")();

    return fetch(`https://api.appfollow.io/apps`, {
      headers: {
        Authorization:
          "Basic " +
          Buffer.from(credentials.apiSecret + ":").toString("base64"),
      },
    })
      .then(jsonify)
      .then(({ apps: collections }) => {
        return Promise.all(
          collections.map((collection) => {
            return fetch(
              `https://api.appfollow.io/apps/app?apps_id=${collection.id}`,
              {
                headers: {
                  Authorization:
                    "Basic " +
                    Buffer.from(credentials.apiSecret + ":").toString("base64"),
                },
              }
            )
              .then((r) => r.json())
              .then(({ apps_app: apps }) =>
                apps.filter(
                  (app) =>
                    [
                      "googleplay",
                      "microsoftstore",
                      "macstore",
                      "itunes",
                    ].includes(app.store) ||
                    (app.store === "amazon" && app.app.kind !== "product")
                )
              );
          })
        )
          .then((result) => result.flat())
          .then((apps) =>
            apps.map((app) => ({
              connectionId,
              store: app.store,
              extId: app.app.ext_id + "",
              name: app.app.title,
              developer: app.app.artist_name,
              icon: app.app.icon,
            }))
          )
          .then((apps) =>
            client()
              .mutate({
                mutation: gql`
                  mutation InsertApps($apps: [apps_insert_input!]!) {
                    insert_apps(
                      objects: $apps
                      on_conflict: {
                        constraint: apps_connectionId_extId_key
                        update_columns: [name, icon, developer]
                      }
                    ) {
                      returning {
                        id
                      }
                    }
                  }
                `,
                variables: {
                  apps,
                },
              })
              .then(({ data }) => data.insert_apps.returning)
          )
          .then((result) =>
            client().mutate({
              mutation: gql`
                mutation DeleteApps($connectionId: uuid!, $skip: [uuid!]) {
                  delete_apps(
                    where: {
                      _and: {
                        _not: { id: { _in: $skip } }
                        connectionId: { _eq: $connectionId }
                      }
                    }
                  ) {
                    returning {
                      id
                    }
                  }
                }
              `,
              variables: {
                connectionId,
                skip: result.map((data) => data.id),
              },
            })
          );
      })
      .catch(error("Couldn't sync apps"));
  });
};

const processReview = (review) => {
  return {
    platformId: review.review_id + "",
    author: review.author,
    review: {
      version: review.app_version,
      timestamp: new Date(review.date + " " + review.time).getTime() / 1000,
      stars: review.rating,
      text: review.content,
      title: review.title,
    },
    reply: review.answer_date && {
      timestamp:
        new Date(review.answer_date + " " + review.answer_time).getTime() /
        1000,
      text: review.answer_text,
    },
    meta: {
      playId: review.store === "gp" ? review.review_id : undefined,
    },
  };
};

export const appfollowReviews = async (req) => {
  const { id, search, page } = req.payload;
  const { cloudId } = req.context;

  return client()
    .query({
      query: gql`
        query ($id: uuid!) {
          apps_by_pk(id: $id) {
            connection {
              id
              cloudId
            }
            extId
          }
        }
      `,
      variables: {
        id,
      },
    })
    .then(
      ({
        data: {
          apps_by_pk: { extId, connection },
        },
      }) =>
        storage.getSecret(`credentials#${connection.id}`).then((credentials) => {
          if (!credentials || connection.cloudId !== cloudId)
            return error("Can't find connection")();

          return fetch(
            `https://api.appfollow.io/reviews?${queryString.stringify({
              ext_id: extId,
              page,
              q: search,
            })}`,
            {
              headers: {
                Authorization:
                  "Basic " +
                  Buffer.from(credentials.apiSecret + ":").toString("base64"),
              },
            }
          )
            .then(jsonify)
            .then((response) => ({
              reviews: response.reviews.list.map(processReview),
              pagination: {
                type: "pages",
                current: response.reviews.page.current,
                total: response.reviews.page.total,
              },
            }));
        })
    );
};

export const appfollowReply = async (req) => {
  const { id, reply } = req.payload;
  const { cloudId } = req.context;

  return client()
    .query({
      query: gql`
        query ($id: uuid!) {
          reviews_by_pk(id: $id) {
            app {
              connection {
                id
                cloudId
              }
              extId
            }
            platformId
            author
            review
          }
        }
      `,
      variables: {
        id,
      },
    })
    .then(
      ({
        data: {
          reviews_by_pk: {
            app: { extId, connection },
            platformId,
            ...review
          },
        },
      }) =>
        storage
          .getSecret(`credentials#${connection.id}`)
          .then(async (credentials) => {
            if (!credentials || connection.cloudId !== cloudId)
              return error("Can't find connection")();

            const text = await replyText(reply, review);
            return fetch(
              `https://api.appfollow.io/reply?${queryString.stringify({
                ext_id: extId,
                review_id: platformId,
                answer_text: text,
              })}`,
              {
                headers: {
                  Authorization:
                    "Basic " +
                    Buffer.from(credentials.apiSecret + ":").toString("base64"),
                },
              }
            )
              .then(jsonify)
              .then((r) => {
                if (r.error) throw new Error(r.error.submsg);
              })
              .then(() => updateReply(id, text));
          })
    );
};
