import api, { route, fetch } from "@forge/api";
import { ApolloClient, InMemoryCache } from "@apollo/client";
import { createHttpLink } from "apollo-link-http";
import { storage } from "@forge/api";
import { gql } from "@apollo/client";
import { Liquid } from "liquidjs";

let apollo_client;
export const client = () => {
  if (!apollo_client)
    apollo_client = new ApolloClient({
      link: createHttpLink({
        uri: "https://reviews.hasura.app/v1/graphql",
        fetch,
        headers: {
          "X-Hasura-Admin-Secret": process.env.HASURA_ADMIN_SECRET,
        },
      }),
      cache: new InMemoryCache(),
    });
  return apollo_client;
};

export const is_admin = () =>
  api
    .asUser()
    .requestJira(route`/rest/api/3/mypermissions?permissions=ADMINISTER`)
    .then((res) => res.json())
    .then((json) => json.permissions.ADMINISTER.havePermission);

export const connect = (context, platform, credentials, account) => {
  const { cloudId, accountId } = context;

  return client()
    .mutate({
      mutation: gql`
        mutation InsertConnection(
          $account: jsonb
          $cloudId: String
          $createdBy: String
          $platform: String
        ) {
          insert_connections_one(
            object: {
              account: $account
              cloudId: $cloudId
              createdBy: $createdBy
              platform: $platform
            }
          ) {
            id
          }
        }
      `,
      variables: {
        cloudId,
        platform,
        createdBy: accountId,
        account,
      },
    })
    .then(
      ({
        data: {
          insert_connections_one: { id },
        },
      }) => storage.setSecret(`credentials#${id}`, credentials)
    );
};

export const jsonify = async (r) => {
  const text = await r.text();
  if (!r.ok) throw new Error(text);
  try {
    return JSON.parse(text);
  } catch {
    return text;
  }
};

export const log = (v) => {
  console.log(v);
  return v;
};

export const error = (message) => {
  return (error) => {
    if (error) console.error(error);
    return {
      error: message,
    };
  };
};

export const replyText = (template, review) => {
  const engine = new Liquid();

  return engine.parseAndRender(template, {
    author: review.author,
    timestamp: new Date(review.review.timestamp * 1000).toISOString(),
    stars: review.review.stars,
    version: review.review.version,
    store: review.app.store,
    appId: review.app.extId,
  });
};

export const updateReply = (id, text) => {
  return client().mutate({
    mutation: gql`
      mutation ($id: uuid!, $reply: jsonb!) {
        update_reviews_by_pk(pk_columns: { id: $id }, _set: { reply: $reply }) {
          reply
        }
      }
    `,
    variables: {
      id,
      reply: {
        timestamp: parseInt(Date.now() / 1000),
        text,
      },
    },
  });
};
