import Resolver from "@forge/resolver";
import jwt from "jsonwebtoken";
import { playAdd, playConnect, playReviews, playReply } from "./play";
import {
  appfollowSync,
  appfollowConnect,
  appfollowReviews,
  appfollowReply,
} from "./appfollow";
import {
  appfiguresConnect,
  appfiguresAuthorize,
  appfiguresSync,
  appfiguresReviews,
  appfiguresReply,
} from "./appfigures";
import { is_admin } from "./helpers";

const resolver = new Resolver();

resolver.define("generateGraphQLToken", async (req) => {
  const { cloudId, accountId, extension } = req.context;
  const projectId = extension?.project?.id;

  return jwt.sign(
    {
      "https://hasura.io/jwt/claims": {
        "x-hasura-allowed-roles": [
          "jira-user",
          ...((await is_admin()) ? ["jira-admin"] : []),
        ],
        "x-hasura-default-role": "jira-user",
        "x-hasura-account-id": accountId,
        "x-hasura-cloud-id": cloudId,
        "x-hasura-project-id":
          projectId && JSON.stringify([projectId.toString()]),
      },
    },
    process.env.HASURA_JWT_SECRET
  );
});

resolver.define("playConnect", playConnect);

resolver.define("playAdd", playAdd);

resolver.define("playReviews", playReviews);

resolver.define("playReply", playReply);

resolver.define("appfiguresAuthorize", appfiguresAuthorize);

resolver.define("appfiguresConnect", appfiguresConnect);

resolver.define("appfiguresSync", appfiguresSync);

resolver.define("appfiguresReviews", appfiguresReviews);

resolver.define("appfiguresReply", appfiguresReply);

resolver.define("appfollowConnect", appfollowConnect);

resolver.define("appfollowSync", appfollowSync);

resolver.define("appfollowReviews", appfollowReviews);

resolver.define("appfollowReply", appfollowReply);

export const handler = resolver.getDefinitions();
