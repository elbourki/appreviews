import {
  is_admin,
  client,
  jsonify,
  connect,
  replyText,
  error,
  updateReply,
} from "./helpers";
import { gql } from "@apollo/client";
import { fetch } from "@forge/api";
import { storage } from "@forge/api";
import queryString from "query-string";
import { languages } from "./data";

const requestGoogle = (connectionId, credentials, packageId, path, init = {}) =>
  fetch(
    `https://androidpublisher.googleapis.com/androidpublisher/v3/applications/${packageId}/${path}`,
    {
      ...init,
      headers: {
        Authorization: "Bearer " + credentials.accessToken,
        ...init.headers,
      },
    }
  )
    .then(jsonify)
    .catch(() =>
      fetch("https://oauth2.googleapis.com/token", {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: new URLSearchParams({
          client_secret: credentials.clientSecret,
          client_id: credentials.clientId,
          grant_type: "refresh_token",
          refresh_token: credentials.refreshToken,
        }).toString(),
      })
        .then(jsonify)
        .then(({ access_token }) => {
          credentials.accessToken = access_token;
          return storage.setSecret(`credentials#${connectionId}`, credentials);
        })
        .then(() =>
          fetch(
            `https://androidpublisher.googleapis.com/androidpublisher/v3/applications/${packageId}/${path}`,
            {
              ...init,
              headers: {
                Authorization: "Bearer " + credentials.accessToken,
                ...init.headers,
              },
            }
          )
        )
        .then(jsonify)
    );

export const playAdd = async (req) => {
  if (!(await is_admin()))
    return error("You don't have the permission to add a connection.")();
  const { id: connectionId, packageId } = req.payload;

  return storage.getSecret(`credentials#${connectionId}`).then((credentials) => {
    if (!credentials) return error("Can't find connection")();

    return requestGoogle(connectionId, credentials, packageId, "edits", {
      method: "POST",
    }).then(({ id }) =>
      requestGoogle(
        connectionId,
        credentials,
        packageId,
        `edits/${id}/details`
      ).then(async ({ defaultLanguage }) => {
        const [
          {
            images: [{ url }],
          },
          { title },
        ] = await Promise.all([
          requestGoogle(
            connectionId,
            credentials,
            packageId,
            `edits/${id}/listings/${defaultLanguage}/icon`
          ),
          requestGoogle(
            connectionId,
            credentials,
            packageId,
            `edits/${id}/listings/${defaultLanguage}`
          ),
        ]);
        await requestGoogle(
          connectionId,
          credentials,
          packageId,
          `edits/${id}`,
          {
            method: "DELETE",
          }
        );
        return client().mutate({
          mutation: gql`
            mutation InsertApp(
              $connectionId: uuid!
              $extId: String!
              $icon: String!
              $name: String!
            ) {
              insert_apps_one(
                on_conflict: {
                  constraint: apps_connectionId_extId_key
                  update_columns: [name, icon, developer]
                }
                object: {
                  connectionId: $connectionId
                  extId: $extId
                  icon: $icon
                  name: $name
                  store: "googleplay"
                }
              ) {
                id
              }
            }
          `,
          variables: {
            connectionId,
            extId: packageId,
            name: title,
            icon: url,
          },
        });
      })
    );
  });
};

export const playConnect = async (req) => {
  if (!(await is_admin()))
    return error("You don't have the permission to add a connection.")();
  const { clientSecret, clientId, code } = req.payload;

  return fetch("https://oauth2.googleapis.com/token", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: new URLSearchParams({
      code,
      client_secret: clientSecret,
      client_id: clientId,
      grant_type: "authorization_code",
      redirect_uri: "urn:ietf:wg:oauth:2.0:oob",
    }).toString(),
  })
    .then(jsonify)
    .then(({ refresh_token: refreshToken, access_token: accessToken }) =>
      connect(
        req.context,
        "play",
        {
          accessToken,
          refreshToken,
          clientId,
          clientSecret,
        },
        {
          clientId,
        }
      )
    )
    .catch(
      error(
        "Unable to connect your developer account, please verify the credentials you entered."
      )
    );
};

const processReview = (review) => {
  const comment = review.comments.find(
    (comment) => comment.userComment
  ).userComment;
  const reply = review.comments.find(
    (comment) => comment.developerComment
  )?.developerComment;

  return {
    platformId: review.reviewId,
    author: review.authorName,
    review: {
      version: comment.appVersionName,
      timestamp: parseInt(comment.lastModified.seconds),
      stars: comment.starRating,
      text: comment.text,
      originalText: comment.originalText,
    },
    reply: reply && {
      timestamp: parseInt(reply.lastModified.seconds),
      text: reply.text,
    },
    meta: {
      playId: review.reviewId,
    },
  };
};

export const playReviews = async (req) => {
  const { id, language, cursor } = req.payload;
  const { cloudId } = req.context;

  const translationLanguage = languages.includes(language)
    ? language
    : undefined;

  return client()
    .query({
      query: gql`
        query ($id: uuid!) {
          apps_by_pk(id: $id) {
            connection {
              id
              cloudId
            }
            extId
          }
        }
      `,
      variables: {
        id,
      },
    })
    .then(
      ({
        data: {
          apps_by_pk: { extId, connection },
        },
      }) =>
        storage.getSecret(`credentials#${connection.id}`).then((credentials) => {
          if (!credentials || connection.cloudId !== cloudId)
            return error("Can't find connection")();

          return requestGoogle(
            connection.id,
            credentials,
            extId,
            `reviews?${queryString.stringify({
              translationLanguage,
              token: cursor,
            })}`
          ).then((response) => ({
            reviews: (response.reviews || []).map(processReview),
            pagination: {
              type: "cursor",
              next: response.tokenPagination?.nextPageToken,
              previous: response.tokenPagination?.previousPageToken,
            },
          }));
        })
    );
};

export const playReply = async (req) => {
  const { id, reply } = req.payload;
  const { cloudId } = req.context;

  return client()
    .query({
      query: gql`
        query ($id: uuid!) {
          reviews_by_pk(id: $id) {
            app {
              connection {
                id
                cloudId
              }
              store
              extId
            }
            platformId
            author
            review
          }
        }
      `,
      variables: {
        id,
      },
    })
    .then(
      ({
        data: {
          reviews_by_pk: {
            app: { extId, connection },
            platformId,
            ...review
          },
        },
      }) =>
        storage
          .getSecret(`credentials#${connection.id}`)
          .then(async (credentials) => {
            if (!credentials || connection.cloudId !== cloudId)
              return error("Can't find connection")();

            const text = await replyText(reply, review);
            return requestGoogle(
              connection.id,
              credentials,
              extId,
              `reviews/${platformId}:reply`,
              {
                method: "POST",
                body: JSON.stringify({
                  replyText: text,
                }),
                headers: {
                  "Content-Type": "application/json",
                },
              }
            ).then(() => updateReply(id, text));
          })
    );
};
