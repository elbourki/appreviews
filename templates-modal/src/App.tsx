import React, { useEffect, useRef, useState } from "react";
import { view } from "@forge/bridge";
import {
  ApolloClient,
  ApolloProvider,
  gql,
  NormalizedCacheObject,
  useMutation,
  useQuery,
} from "@apollo/client";
import {
  createClient,
  Error as ErrorComponent,
  Loading,
} from "@app-reviews/shared";
import { __RouterContext } from "react-router";
import styles from "./App.module.scss";
import Button, { LoadingButton } from "@atlaskit/button";
import Form, {
  ErrorMessage,
  Field,
  FormFooter,
  HelperMessage,
} from "@atlaskit/form";
import TextArea from "@atlaskit/textarea";
import { Code } from "@atlaskit/code";
import { CreatableSelect } from "@atlaskit/select";

function Templates() {
  const [selectedId, setSelectedId] = useState<string | null>(null);
  const [temp, setTemp] = useState<any>(null);
  const setFieldValueRef = useRef<any>(null);
  const { loading, data, refetch } = useQuery(
    gql`
      query {
        templates {
          id
          name
          template
        }
      }
    `
  );
  const [remove, { loading: removing }] = useMutation(
    gql`
      mutation ($id: uuid!) {
        delete_templates_by_pk(id: $id) {
          id
        }
      }
    `
  );
  const [insert, { loading: inserting }] = useMutation(
    gql`
      mutation ($name: String!, $template: String!) {
        insert_templates_one(object: { name: $name, template: $template }) {
          id
        }
      }
    `
  );
  const [update, { loading: updating }] = useMutation(
    gql`
      mutation ($id: uuid!, $template: String!) {
        update_templates_by_pk(
          pk_columns: { id: $id }
          _set: { template: $template }
        ) {
          id
        }
      }
    `
  );

  const templates = [
    ...(data?.templates || []).map((template) => ({
      label: template.name,
      value: template.id,
      template: template.template,
    })),
    temp,
  ].filter(Boolean);
  const selected =
    templates.find((template) => template.value === selectedId) || null;

  const create = (name) => {
    const temp = {
      label: name,
      value: "new",
      template: "",
    };
    setTemp(temp);
    setSelectedId(temp.value);
  };

  const change = (value) => {
    setSelectedId(value.value);
  };

  const handleSubmit = (formState) => {
    const data = {
      template: formState.template,
    };
    if (temp === selected) {
      insert({
        variables: { ...data, name: selected.label },
      }).then(
        ({
          data: {
            insert_templates_one: { id },
          },
        }) =>
          refetch().then(() => {
            setTemp(null);
            setSelectedId(id);
          })
      );
    } else {
      update({
        variables: { ...data, id: selected.value },
      }).then(() => refetch());
    }
  };

  useEffect(() => {
    if (setFieldValueRef.current)
      setFieldValueRef.current("template", selected?.template || "");
  }, [selectedId, data, temp, setFieldValueRef.current]);

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <h3>Manage templates</h3>
      </div>
      <Form onSubmit={handleSubmit}>
        {({ formProps, setFieldValue }) => {
          setFieldValueRef.current = setFieldValue;

          return (
            <form {...formProps}>
              <CreatableSelect
                placeholder="Select template..."
                onChange={change}
                onCreateOption={create}
                options={templates}
                value={selected}
              />
              <Field
                label="Reply template"
                isRequired
                isDisabled={!selected}
                name="template"
                validate={(value: any) => {
                  if (value.length > 350) return "TOO_LONG";
                }}
                defaultValue={""}
              >
                {({ fieldProps, error }: any) => (
                  <>
                    <TextArea {...fieldProps} minimumRows={3} />
                    {error === "TOO_LONG" && (
                      <ErrorMessage>
                        Your reply can only contain 350 characters at most.
                      </ErrorMessage>
                    )}
                    <HelperMessage>
                      You can mix variables, conditions and filters to build a
                      personalized template!
                      <br />
                      Here are some examples to get you started:
                    </HelperMessage>
                    <HelperMessage>
                      <ul>
                        <li>
                          <Code>{`{{ author | split: " " | first | capitalize }}`}</Code>
                        </li>
                        <li>
                          <Code>{`{{ timestamp | date: "%a, %b %d, %y" }}`}</Code>
                        </li>
                        <li>
                          <Code>{`{% if stars < 3 %}Would you please consider updating your rating?{% endif %}`}</Code>
                        </li>
                      </ul>
                    </HelperMessage>
                  </>
                )}
              </Field>
              <FormFooter>
                {temp !== selected && (
                  <LoadingButton
                    appearance="danger"
                    className={styles["pull-left"]}
                    onClick={() =>
                      remove({
                        variables: {
                          id: selected.value,
                        },
                      })
                        .then(() => refetch())
                        .then(({ data: { templates } }) =>
                          setSelectedId(templates[0]?.id)
                        )
                    }
                    isLoading={removing}
                    isDisabled={!selected}
                  >
                    Delete
                  </LoadingButton>
                )}
                <Button onClick={() => view.close()} appearance="subtle">
                  Close
                </Button>
                <LoadingButton
                  type="submit"
                  appearance="primary"
                  isLoading={loading || updating || inserting}
                  isDisabled={!selected}
                >
                  {temp === selected ? "Add template" : "Update template"}
                </LoadingButton>
              </FormFooter>
            </form>
          );
        }}
      </Form>
    </div>
  );
}

function App() {
  const [client, setClient] =
    useState<ApolloClient<NormalizedCacheObject> | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    view
      .getContext()
      .then((context) =>
        setClient(createClient((context as any).extension.modal.token))
      )
      .catch(() => setError(true))
      .then(() => setLoading(false));
  }, []);

  if (error) return <ErrorComponent />;

  if (loading) return <Loading />;

  return (
    <ApolloProvider client={client as any}>
      <Templates />
    </ApolloProvider>
  );
}

export default App;
