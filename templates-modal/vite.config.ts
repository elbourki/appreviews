import { defineConfig } from "vite";
import reactRefresh from "@vitejs/plugin-react-refresh";
import inject from "@rollup/plugin-inject";

export default defineConfig({
  plugins: [
    reactRefresh(),
    inject({
      __DEV__: ["@app-reviews/shared", "__DEV__"],
    }),
  ],
  base: "./",
  define: {
    "process.env": process.env,
  },
  server: {
    port: 3005,
    fs: {
      allow: [".."],
    },
  },
});
